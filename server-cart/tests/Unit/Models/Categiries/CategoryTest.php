<?php

namespace Tests\Unit\Models\Categiries;

use Tests\TestCase;
use App\Models\Category;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CategoryTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_it_many_child ()
    {
      $category = factory(Category::class)->create();

      $category->children()->save(
        factory(Category::class)->create()
      );

        $this->assertTrue(Category::class, $category->children->first());
    }
}
