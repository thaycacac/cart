<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ProductTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    DB::table('products')->insert([
      'name'=> $name = Str::random(10),
      'slug'=> Str::slug($name),
      'description'=> Str::random(100),
      'price'=> 10,
      'created_at' => now(),
      'updated_at' => now()
    ]);
  }
}
