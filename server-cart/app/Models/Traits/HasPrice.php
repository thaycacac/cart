<?php

namespace App\Models\Traits;

trait HasPrice {
  public function getFormatedPriceAttribute() {
    return $this->price . '$';
  }
}
