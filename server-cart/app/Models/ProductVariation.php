<?php

namespace App\Models;

use App\Models\Traits\HasPrice;
use App\Models\ProductVariationType;
use Illuminate\Database\Eloquent\Model;

class ProductVariation extends Model
{
  use HasPrice;

  public function getPriceAttribute($value) {
    if($value === null) {
      return '0';
    }
    return $value;
  }

  public function type() {
    return $this->hasOne(ProductVariationType::class, 'id', 'product_variation_type_id');
  }
}
